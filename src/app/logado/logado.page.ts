import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, NavigationExtras } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-logado',
  templateUrl: './logado.page.html',
  styleUrls: ['./logado.page.scss'],
})
export class LogadoPage implements OnInit {

  constructor(private http: HttpClient, private router: Router, public alertController: AlertController) { }

  public tamanhos;
  public selected;

  ngOnInit() {
    this.getTamanhos();
  }

  getTamanhos() {
    this.http.get('http://104.196.102.231/tamanhos').subscribe(data => {
      this.tamanhos = data;
    }, error => {
      this.presentAlert();
    });
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Atenção!',
      subHeader: '',
      message: 'Problema ao carregar tamanho',
      buttons: ['OK']
    });

    await alert.present();
  }
}
