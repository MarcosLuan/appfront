
import { Component, OnInit } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  username;
  password;

  constructor(private http: Http, private router: Router, public alertController: AlertController) { }

  ngOnInit() {
  }

  login() {
    this.http
      .post('http://104.196.102.231/logon', { userName: this.username, password: this.password })
      // .post('http://localhost:8080/logon', { userName: this.username, password: this.password })
      .subscribe(data => {
        this.router.navigateByUrl('/logado');
      }, error => {
        this.presentAlert();
      });
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Atenção!',
      subHeader: '',
      message: 'Usuário não encontrado',
      buttons: ['OK']
    });

    await alert.present();
  }
}
