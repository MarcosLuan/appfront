import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { IonicModule } from '@ionic/angular';
import { SaboresPage } from './sabores.page';
import { LogadoPageModule } from '../logado/logado.module';

const routes: Routes = [
  {
    path: '',
    component: SaboresPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SaboresPage]
})
export class SaboresPageModule {}
