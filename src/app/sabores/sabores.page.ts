// import { SaboresPage } from './../sabores/sabores.page';
// import { Component, OnInit } from '@angular/core';
// import { HttpClient } from '@angular/common/http';
// import { Router, NavigationExtras } from '@angular/router';
// import { AlertController } from '@ionic/angular';

// @Component({
//   selector: 'app-sabores',
//   templateUrl: './sabores.page.html',
//   styleUrls: ['./sabores.page.scss'],
// })
// export class SaboresPage implements OnInit {

//   constructor(
//     private http: HttpClient,
//     private router: Router,
//     public alertController: AlertController,
//     public saboresPage: SaboresPage
//     ) { }

//   public sabores;

//   ngOnInit() {
//     this.getSabores();
//   }

//   getSabores() {
//     // tslint:disable-next-line:no-debugger
//     debugger;
//     this.http.get('http://104.196.102.231/sabores/' + this.saboresPage.selected).subscribe(data => {
//       this.sabores = data;
//     }, error => {
//       this.presentAlert();
//     });
//   }

//   async presentAlert() {
//     const alert = await this.alertController.create({
//       header: 'Atenção!',
//       subHeader: '',
//       message: 'Problema ao carregar sabores',
//       buttons: ['OK']
//     });

//     await alert.present();
//   }
// }

import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, NavigationExtras } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-sabores',
  templateUrl: './sabores.page.html',
  styleUrls: ['./sabores.page.scss'],
})
export class SaboresPage implements OnInit {

  constructor(private http: HttpClient, private router: Router, public alertController: AlertController) { }

  public tamanhos;
  public selected;

  ngOnInit() {
    this.getTamanhos();
  }

  getTamanhos() {
    this.http.get('http://104.196.102.231/sabores/3').subscribe(data => {
      this.tamanhos = data;
    }, error => {
      this.presentAlert();
    });
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Atenção!',
      subHeader: '',
      message: 'Problema ao carregar sabores',
      buttons: ['OK']
    });

    await alert.present();
  }
}
